

### JSON Wallet RPC API Docs

Documentation on how to interact with `amity-wallet-rpc`


---

---

### JSON RPC Methods:    

---

---

###**`get_balance`** 

Return the wallet's balance.

**Inputs:**

* **`account_index`** - *unsigned int*; Return balance for this account.

* **`address_indices`** - *array of unsigned int*; (Optional) Return balance detail for those subaddresses.

**Outputs:**

* **`balance`** - *unsigned int*; The total balance of the current amity-wallet-rpc in session.

* **`unlocked_balance`** - *unsigned int*; Unlocked funds are those funds that are sufficiently deep enough in the Amity blockchain to be considered safe to spend.

* **`multisig_import_needed`** - **boolean**; True if importing multisig data is needed for returning a correct balance.

* **`per_subaddress`** - *array of subaddress information*; Balance information for each subaddress in an account.

    * **`address_index`** - *unsigned int*; Index of the subaddress in the account.

    * **`address`** - *string*; Address at this index. Base58 representation of the public keys.

    * **`balance`** - *unsigned int*; Balance for the subaddress (locked or unlocked).

    * **`unlocked_balance`** - *unsigned int*; Unlocked balance for the subaddress.

    * **`label`** - *string*; Label for the subaddress.

    * **`num_unspent_outputs`** - *unsigned int*; Number of unspent outputs available for the subaddress.


!!! example
    **`get_balance`** 

    ```
    $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_balance","params":{"account_index":0,"address_indices":[0,1]}}' -H 'Content-Type: application/json'
    {
    "id": "0",
    "jsonrpc": "2.0",
    "result": {
        "balance": 157443303037455077,
        "multisig_import_needed": false,
        "per_subaddress": [{
        "address": "amitGvbEpdH6vcmWR3qXd9XXyud78DQfM84D2P9NMpJEV9iZFTTrmP5f6kUwWVmfNBXyMs4AufNUoUrGsqwfUn1L695cS6EkwC",
        "address_index": 0,
        "balance": 157360317826,
        "label": "Primary account",
        "num_unspent_outputs": 5281,
        "unlocked_balance": 157360317826
        },{
        "address": "amitGvbEpdH6vcmWR3qXd9XXyud78DQfM84D2P9NMpJEV9iZFTTrmP5f6kUwWVmfNBXyMs4AufNUoUrGsqwfUn1L695cS6EkwC",
        "address_index": 1,
        "balance": 59985211,
        "label": "",
        "num_unspent_outputs": 1,
        "unlocked_balance": 59985211
        }],
        "unlocked_balance": 157443303037
      }
    }
    ```
---

---

###**`get_address`**

Return the wallet's addresses for an account. Optionally filter for specific set of subaddresses.

**Inputs:**

* **`account_index`** - *unsigned int*; Return subaddresses for this account.

* **`address_index`** - *array of unsigned int*; (Optional) List of subaddresses to return from an account.

**Outputs:**

* **`address`** - *string*; The 95-character hex address string of the amity-wallet-rpc in session.

* **`addresses`** array of addresses informations

    * **`address`** - *string*; The 95-character hex (sub)address string.

    * **`label`** - *string*; Label of the (sub)address.

    * **`address_index`** - *unsigned int*; index of the subaddress.

    * **`used`** - **boolean**; states if the (sub)address has already received funds.


!!! example
    **`get_address`**

    ```
    $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_address","params":{"account_index":0,"address_index":[0,1,4]}}' -H 'Content-Type: application/json'
    {
    "id": "0",
    "jsonrpc": "2.0",
    "result": {
        "address": "amitGvbEpdH6vcmWR3qXd9XXyud78DQfM84D2P9NMpJEV9iZFTTrmP5f6kUwWVmfNBXyMs4AufNUoUrGsqwfUn1L695cS6EkwC",
        "addresses": [{
        "address": "amitGvbEpdH6vcmWR3qXd9XXyud78DQfM84D2P9NMpJEV9iZFTTrmP5f6kUwWVmfNBXyMs4AufNUoUrGsqwfUn1L695cS6EkwC",
        "address_index": 0,
        "label": "Primary account",
        "used": true
        },{
        "address": "amitQNi7kEVB7N2j9CXE7ZXoTg5EJ7i5zT3XzzJHdKyKfF93t2tFdgcLHRvfYzB4WR2Pu2M6LvzZpPQjLu6Vva9934wS5t3VHM",
        "address_index": 1,
        "label": "",
        "used": true
        },{
        "address": "amitVF4yZ13M9C6UPWVBuUWCafw9FLKXAYWg6VF5s2tyMCdLvCt3p5k4EefdwnhMuUWFhjbGahcVC7WGjhzAjvmy2ozmCiS9Kk",
        "address_index": 4,
        "label": "mitties",
        "used": true
        }]
      }
    }
    ```
---

---

###**`get_address_index`**

Get account and address indexes from a specific (sub)address

**Inputs:**

* **`address`** - *string*; (sub)address to look for.

**Outputs:**

* **`index`** - subaddress informations

    * **`major`** - *unsigned int*; Account index.

    * **`minor`** *unsigned int*; Address index.

!!! example    
    **`get_address_index`**

    ```
    $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_address_index","params":     {"address":"amitGvbEpdH6vcmWR3qXd9XXyud78DQfM84D2P9NMpJEV9iZFTTrmP5f6kUwWVmfNBXyMs4AufNUoUrGsqwfUn1L695cS6EkwC"}}' -H 'Content-Type: application/json'
    {
    "id": "0",
    "jsonrpc": "2.0",
    "result": {
        "index": {
        "major": 0,
        "minor": 1
        }
      }
    }
    ```

---

---

###**`create_address`** 

Create a new address for an account. Optionally, label the new address.

**Inputs:**

* **`account_index`** - *unsigned int*; Create a new address for this account.

* **`label`** - *string*; (Optional) Label for the new address.

**Outputs:**

* **`address`** - *string*; Newly created address. Base58 representation of the public keys.

* **`address_index`** - *unsigned int*; Index of the new address under the input account.

!!! example    
    **`create_address`**

    ```
    $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"create_address","params":{"account_index":0,"label":"new-sub"}}' -H 'Content-Type: application/json'
    {
    "id": "0",
    "jsonrpc": "2.0",
    "result": {
        "address": "amitGvbEpdH6vcmWR3qXd9XXyud78DQfM84D2P9NMpJEV9iZFTTrmP5f6kUwWVmfNBXyMs4AufNUoUrGsqwfUn1L695cS6EkwC",
        "address_index": 5
      }
    }
    ```


---

---

###**`label_address`**

Label an address.

**Inputs:**

* **`index`** - *subaddress index*; JSON Object containing the major & minor address index:
    
    * **`major`** - *unsigned int*; Account index for the subaddress.

    * **`minor`** - *unsigned int*; Index of the subaddress in the account.

* **`label`** - *string*; Label for the address.

**Outputs:**

* *None*

!!! example    
    **`label_address`**

    ```
    $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"label_address","params":{"index":{"major":0,"minor":5},"label":"myLabel"}}' -H 'Content-Type: application/json'
    {
    "id": "0",
        "jsonrpc": "2.0",
        "result": {
      }
    }
    ```
---

---

###**`get_accounts`**

Get all accounts for a wallet. Optionally filter accounts by tag.

**Inputs:**

* **`tag`** - *string*; (Optional) Tag for filtering accounts.

**Outputs:**

* **`subaddress_accounts`** - array of subaddress account information:

    * **`account_index`** - *unsigned int*; Index of the account.

    * **`balance`** - *unsigned int*; Balance of the account (locked or unlocked).

    * **`base_address`** - *string*; Base64 representation of the first subaddress in the account.

    * **`label`** - *string*; (Optional) Label of the account.

    * **`tag`** - *string*; (Optional) Tag for filtering accounts.

    * **`unlocked_balance`** - *unsigned int*; Unlocked balance for the account.

* **`total_balance`** - *unsigned int*; Total balance of the selected accounts (locked or unlocked).

* **`total_unlocked_balance`** - *unsigned int*; Total unlocked balance of the selected accounts.

!!! example    
    **`get_accounts`**

    ```
    $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_accounts","params":{"tag":"mittiesOne"}}' -H 'Content-Type: application/json'
    {
    "id": "0",
    "jsonrpc": "2.0",
    "result": {
        "subaddress_accounts": [{
        "account_index": 0,
        "balance": 157663195572,
        "base_address": "amitGvbEpdH6vcmWR3qXd9XXyud78DQfM84D2P9NMpJEV9iZFTTrmP5f6kUwWVmfNBXyMs4AufNUoUrGsqwfUn1L695cS6EkwC",
        "label": "Primary account",
        "tag": "mittiesOne",
        "unlocked_balance": 157443303037
        },{
        "account_index": 1,
        "balance": 0,
        "base_address": "asubVF4yZ13M9C6UPWVBuUWCafw9FLKXAYWg6VF5s2tyMCdLvCt3p5k4EefdwnhMuUWFhjbGahcVC7WGjhzAjvmy2ozmCiS9Kk",
        "label": "Secondary account",
        "tag": "mittiesOne",
        "unlocked_balance": 0
        }],
        "total_balance": 157663195572,
        "total_unlocked_balance": 157443303037
      }
    }
    ```
---

---

###**`create_account`**

Create a new account with an optional label.

**Inputs:**

* **`label`** - *string*; (Optional) Label for the account.

**Outputs:**

* **`account_index`** - *unsigned int*; Index of the new account.

* **`address`** - *string*; Address for this account. Base58 representation of the public keys.

!!! example    
    **`create_account`**

    ```
    $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"create_account","params":{"label":"Secondary     account"}}' -H 'Content-Type: application/json'
    {
    "id": "0",
    "jsonrpc": "2.0",
    "result": {
        "account_index": 1,
        "address": "amitGvbEpdH6vcmWR3qXd9XXyud78DQfM84D2P9NMpJEV9iZFTTrmP5f6kUwWVmfNBXyMs4AufNUoUrGsqwfUn1L695cS6EkwC"
      }
    }
    ```
---

---

###**`label_account`**

Label an account.

**Inputs:**

* **`account_index`** - *unsigned int*; Apply label to account at this index.

* **`label`** - *string*; Label for the account.

**Outputs:**

* *None*

!!! example    
    **`label_account`**

    ```
    $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"label_account","params":{"account_index":0,"label":"Primary account"}}' -H 'Content-Type: application/json'
    {
    "id": "0",
    "jsonrpc": "2.0",
    "result": {
        "account_tags": [{
        "accounts": [0,1],
        "label": "",
        "tag": "mittiesOne"
        }]
      }
    }
    ```
---

---

###**`get_account_tags`**

Get a list of user-defined account tags.

**Inputs:**

* *None*

**Outputs:**

* **`account_tags`** - array of account tag information:

    * **`tag`** - *string*; Filter tag.

    * **`label`** - *string*; Label for the tag.

    * **`accounts`** - *array of int*; List of tagged account indices.


!!! example
    **`get_account_tags`**

    ```
    $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_account_tags","params":""}' -H 'Content-Type: application/json'
    {
    "id": "0",
    "jsonrpc": "2.0",
    "result": {
        "account_tags": [{
        "accounts": [0],
        "label": "Test tag",
        "tag": "myTag"
        }]
      }
    }
    ```

---

---

###**`tag_accounts`**

Apply a filtering tag to a list of accounts.

**Inputs:**

* **`tag`** - *string*; Tag for the accounts.

* **`accounts`** - *array of unsigned int*; Tag this list of accounts.

**Outputs**

* *None*

!!! example
    **`tag_accounts`**
    
    ```
      $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"tag_accounts","params":{"tag":"myTag","accounts":[0,1]}}' -H 'Content-Type: application/json'
      {
        "id": "0",
        "jsonrpc": "2.0",
        "result": {
        }
      }
    ```

---

---

###**`untag_accounts`**

Remove filtering tag from a list of accounts.

**Inputs:**

* **`accounts`** - *array of unsigned int*; Remove tag from this list of accounts.

**Outputs:**

* *None*

!!! example
    **`untag_accounts`**

    ```
       $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"untag_accounts","params":{"accounts":[1]}}' -H 'Content-Type: application/json'
       {
         "id": "0",
         "jsonrpc": "2.0",
         "result": {
       }
     }
    ```

---

---

###**`set_account_tag_description`**

Set description for an account tag.

**Inputs:**

* **`tag`** - *string*; Set a description for this tag.

* **`description`** - *string*; Description for the tag.

**Outputs:**

* *None*

!!! example
    **`set_account_tag_description`**

    ```
      $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"set_account_tag_description","params":{"tag":"myTag","description":"Test tag"}}' -H 'Content-Type: application/json'
      {
      "id": "0",
      "jsonrpc": "2.0",
      "result": {
      }
    }
    ```  

---

---

###**`get_height`**

Returns the wallet's current block height.

**Inputs:**

* *None*

**Outputs:**

* **`height`** - *unsigned int*; The current amity-wallet-rpc's blockchain height. If the wallet has not been opened for some time, you may need to wait for it to catch up with the daemon.

!!! example
    **`get_height`**

    ```
      $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_height"}' -H 'Content-Type: application/json'
      {
      "id": "0",
      "jsonrpc": "2.0",
      "result": {
      "height": 25869
      }
    }
    ```

---

---

###**`transfer`**

Send XAM to one or multiple recipients.

**Inputs:**

* **`destinations`** - array of destinations to receive XAM:

    * **`amount`** - *unsigned int*; Amount to send to each destination, in atomic units.

    * **`address`** - *string*; Destination public address.

* **`account_index`** - *unsigned int*; (Optional) Transfer from this account index. (Defaults to 0)

* **`subaddr_indices`** - *array of unsigned int*; (Optional) Transfer from this set of subaddresses. (Defaults to empty - all indices)

* **`priority`** - *unsigned int*; Set a priority for the transaction. Accepted Values are: 0-3 for: default, unimportant, normal, elevated, priority.

* **`mixin`** - *unsigned int*; Number of outputs from the blockchain to mix with (0 means no mixing).

* **`unlock_time`** - *unsigned int*; Number of blocks before the amity can be spent (0 to not add a lock).

* **`payment_id`** - *string*; (Optional) Random 32-byte/64-character hex string to identify a transaction.

* **`get_tx_key`** - **boolean**; (Optional) Return the transaction key after sending.

* **`do_not_relay`** - **boolean**; (Optional) If true, the newly created transaction will not be relayed to the amity network. (Defaults to false)

* **`get_tx_hex`** - **boolean**; Return the transaction as hex string after sending (Defaults to false)

* **`get_tx_metadata`** - **boolean**; Return the metadata needed to relay the transaction. (Defaults to false)

**Outputs:**

* **`amount`** - Amount transferred for the transaction.

* **`fee`** - Integer value of the fee charged for the txn.

* **`multisig_txset`** - Set of multisig transactions in the process of being signed (empty for non-multisig).

* **`tx_blob`** - Raw transaction represented as hex string, if get_tx_hex is true.

* **`tx_hash`** - *string*; for the publically searchable transaction hash.

* **`tx_key`** - *string*; for the transaction key if get_tx_key is true, otherwise, blank string.

* **`tx_metadata`** - Set of transaction metadata needed to relay this transfer later, if get_tx_metadata is true.

* **`unsigned_txset`** - *string*;. Set of unsigned tx for cold-signing purposes.

!!! example
    **`transfer`**

    ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"transfer","params":{"destinations":[{"amount":230000000000,"address":"amitGvbEpdH6vcmWR3qXd9XXyud78DQfM84D2P9NMpJEV9iZFTTrmP5f6kUwWVmfNBXyMs4AufNUoUrGsqwfUn1L695cS6EkwC"},{"amount":690000000000,"address":"asubVF4yZ13M9C6UPWVBuUWCafw9FLKXAYWg6VF5s2tyMCdLvCt3p5k4EefdwnhMuUWFhjbGahcVC7WGjhzAjvmy2ozmCiS9Kk"}],"account_index":0,"subaddr_indices":[0],"priority":0,"get_tx_key": true}}' -H 'Content-Type: application/json'
        {
        "id": "0",
        "jsonrpc": "2.0",
        "result": {
          "amount": 300000000000,
          "fee": 86897600000,
          "multisig_txset": "",
          "tx_blob": "",
          "tx_hash": "ea9ecf7108c103d3362bd2f0c4b75be25a00b14ae97663438de4f72be395b770",
          "tx_key": "25c3d665c9d3684aab7335ff466f13421bdf8df98b2cd02e1ba2638485afd1c70e236c4bdd71c0a2da6338d82c823f911f1cb511db88b7b969c44893b9d1648ec499eadaca6239d7251490e4bf1bbf9f6ffacffdcdc4d6b4e02ce92d4a452e5d009d8ec20045c80c99fbbbf15b549df8856205a4c7bacd00cb75850b198c5803",
          "tx_metadata": "",
          "unsigned_txset": ""
        }
      }
    ```

---

---

###**`transfer_split`**

Same as transfer, but can split into more than one tx if necessary.

**Inputs:**

* **`destinations`** - array of destinations to receive XAM:

    * **`amount`** - *unsigned int*; Amount to send to each destination, in atomic units.

    * **`address`** - *string*; Destination public address.

* **`account_index`** - *unsigned int*; (Optional) Transfer from this account index. (Defaults to 0)

* **`subaddr_indices`** - *array of unsigned int*; (Optional) Transfer from this set of subaddresses. (Defaults to empty - all indices)

* **`mixin`** - *unsigned int*; Number of outputs from the blockchain to mix with (0 means no mixing).

* **`unlock_time`** - *unsigned int*; Number of blocks before the amity can be spent (0 to not add a lock).

* **`payment_id`** - *string*; (Optional) Random 32-byte/64-character hex string to identify a transaction.

* **`get_tx_keys`** - **boolean**; (Optional) Return the transaction keys after sending.

* **`priority`** - *unsigned int*; Set a priority for the transactions. Accepted Values are: 0-3 for: default, unimportant, normal, elevated, priority.

* **`do_not_relay`** - **boolean**; (Optional) If true, the newly created transaction will not be relayed to the amity network. (Defaults to false)

* **`get_tx_hex`** - **boolean**; Return the transactions as hex string after sending

* **`get_tx_metadata`** - *boolean*; Return list of transaction metadata needed to relay the transfer later.

**Outputs:**

* **`tx_hash_list`** - *array of: string*. The tx hashes of every transaction.

* **`tx_key_list`** - *array of: string*. The transaction keys for every transaction.

* **`amount_list`** - *array of: integer*. The amount transferred for every transaction.

* **`fee_list`** - *array of: integer*. The amount of fees paid for every transaction.

* **`tx_blob_list`** - *array of: string*. The tx as hex string for every transaction.

* **`tx_metadata_list`** - *array of: string*. List of transaction metadata needed to relay the transactions later.

* **`multisig_txset`** - *string*. The set of signing keys used in a multisig transaction (empty for non-multisig).

* **`unsigned_txset`** - *string*. Set of unsigned tx for cold-signing purposes.

!!! example
    **`transfer_split`**

    ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"transfer_split","params":{"destinations":[{"amount":2300000000000,"address":"amitGvbEpdH6vcmWR3qXd9XXyud78DQfM84D2P9NMpJEV9iZFTTrmP5f6kUwWVmfNBXyMs4AufNUoUrGsqwfUn1L695cS6EkwC"},{"amount":6900000000000,"address":"asubVF4yZ13M9C6UPWVBuUWCafw9FLKXAYWg6VF5s2tyMCdLvCt3p5k4EefdwnhMuUWFhjbGahcVC7WGjhzAjvmy2ozmCiS9Kk"}],"account_index":0,"subaddr_indices":[0],"priority":0,"get_tx_keys": true}}' -H 'Content-Type: application/json'
      {
        "id": "0",
        "jsonrpc": "2.0",
        "result": {
          "amount_list": [3000000000000],
          "fee_list": [473710000],
          "multisig_txset": "",
          "tx_hash_list": ["4a7375d5e70cdf8fb7a387cd0dcdc1af3f66577ac53c0882b771bd18df5ca540"],
          "tx_key_list": ["7168be68a5b455c0f9064bb84cdaef01b5c48d73a0b714f2ca19ea6c8ff592417e606addea6deb1dbb27075fb6530e2969f75681ffcbfc4075677b94a8c9197963ae38fa6f543ee68f0a4c4bbda4c652a2c03c5c453ba2f3af39538f00b28e980ea0850974dde30b51c004960101dbc61b534cbbdff0d5af9dba023090debd06"],
          "unsigned_txset": ""
        }
      }
    ```

---

---

###**`sign_transfer`**

Sign a transaction created on a read-only wallet (in cold-signing process)

**Inputs:**

* **`unsigned_txset`** - *string*. Set of unsigned tx returned by "transfer" or "transfer_split" methods.

* **`export_raw`** - **boolean**; (Optional) If true, return the raw transaction data. (Defaults to false)

**Outputs:**

* **`signed_txset`** - *string*. Set of signed tx to be used for submitting transfer.

* **`tx_hash_list`** - *array of: string*. The tx hashes of every transaction.

* **`tx_raw_list`** - *array of: string*. The tx raw data of every transaction.

In the following example, first we need to generate an unsigned_txset with a read only wallet before we can go about signing it:

!!! example
    **First:** Generate unsigned_txset using the above "transfer" method with a read-only wallet:

      ```
        curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"transfer","params":{"destinations":[{"amount":1000000000000,"address":"amitGvbEpdH6vcmWR3qXd9XXyud78DQfM84D2P9NMpJEV9iZFTTrmP5f6kUwWVmfNBXyMs4AufNUoUrGsqwfUn1L695cS6EkwC"}],"account_index":0,"subaddr_indices":[0],"priority":0,"do_not_relay":true,"get_tx_hex":true}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "amount": 1000000000000,
            "fee": 15202740000,
            "multisig_txset": "",
            "tx_blob": "...long_hex...",
            "tx_hash": "c648b4b21eaca0a04961dbf6e0f828eea90902e5ce4ec21315de861d0a4",
            "tx_key": "",
            "tx_metadata": "",
            "unsigned_txset": "...long_hex..."
          }
        }
      ```
    **Second:** Sign tx using the previously generated unsigned_txset

      ```
        curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"sign_transfer","params":{"unsigned_txset":...long_hex..."}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "signed_txset": "...long_hex...",
            "tx_hash_list": ["ff2e2d49fbfb1c9a55754f786576e171c8bf21b463a74438df604b7fa6cebc6d"]
          }
        }
      ```  

---

---


###**`submit_transfer`**

Submit a previously signed transaction on a read-only wallet (in cold-signing process).

**Inputs:**

* **`tx_data_hex`** - *string*; Set of signed tx returned by "sign_transfer"

Outputs:

* **`tx_hash_list`** - *array of: string*. The tx hashes of every transaction.


In the following example, we submit the transfer using the signed_txset generated above:

!!! example

    ```
      curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"submit_transfer","params":{"tx_data_hex":...long_hex..."}}' -H 'Content-Type: application/json'
      {
        "id": "0",
        "jsonrpc": "2.0",
        "result": {
          "tx_hash_list": ["40fad7c828bb383ac02648732f7afce9adc520ba5629e1f5d9c03f584ac53d74"]
        }
      }
    ```

---

---

###**`sweep_dust`**

Send all dust outputs back to the wallet's, to make them easier to spend (and mix).

**Inputs:**

* **`get_tx_keys`** - **boolean**; (Optional) Return the transaction keys after sending.

* **`do_not_relay`** - **boolean**; (Optional) If true, the newly created transaction will not be relayed to the amity network. (Defaults to false)

* **`get_tx_hex`** - **boolean**; (Optional) Return the transactions as hex string after sending. (Defaults to false)

* **`get_tx_metadata`** - **boolean**; (Optional) Return list of transaction metadata needed to relay the transfer later. (Defaults to false)

**Outputs:**

* **`tx_hash_list`** - *array of: string*. The tx hashes of every transaction.

* **`tx_key_list`** - *array of: string*. The transaction keys for every transaction.

* **`amount_list`** - *array of: integer*. The amount transferred for every transaction.

* **`fee_list`** - *array of: integer*. The amount of fees paid for every transaction.

* **`tx_blob_list`** - *array of: string*. The tx as hex string for every transaction.

* **`tx_metadata_list`** - *array of: string*. List of transaction metadata needed to relay the transactions later.

* **`multisig_txset`** - *string*. The set of signing keys used in a multisig transaction (empty for non-multisig).

* **`unsigned_txset`** - *string*. Set of unsigned tx for cold-signing purposes.

!!! example

    In this example, sweep_dust returns nothing because there are no funds to sweep:

      ```  
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"sweep_dust","params":{"get_tx_keys":true}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "multisig_txset": "",
            "unsigned_txset": ""
          }
        }
      ```

---

---

###**`sweep_all`**

Send all unlocked balance to an address.

**Inputs:**

* **`address`** - *string*; Destination public address.

* **`account_index`** - *unsigned int*; Sweep transactions from this account.

* **`subaddr_indices`** - *array of unsigned int*; (Optional) Sweep from this set of subaddresses in the account.

* **`priority`** - *unsigned int*; (Optional) Priority for sending the sweep transfer, partially determines fee.

* **`mixin`** - *unsigned int*; Number of outputs from the blockchain to mix with (0 means no mixing).

* **`unlock_time`** - *unsigned int*; Number of blocks before the amity can be spent (0 to not add a lock).

* **`payment_id`** - *string*; (Optional) Random 32-byte/64-character hex string to identify a transaction.

* **`get_tx_keys`** - **boolean**; (Optional) Return the transaction keys after sending.

* **`below_amount`** - *unsigned int*; (Optional) Include outputs below this amount.

* **`do_not_relay`** - **boolean**; (Optional) If true, do not relay this sweep transfer. (Defaults to false)

* **`get_tx_hex`** - **boolean**; (Optional) return the transactions as hex encoded string. (Defaults to false)

* **`get_tx_metadata`** - **boolean**; (Optional) return the transaction metadata as a string. (Defaults to false)

**Outputs:**

* **`tx_hash_list`** - *array of: string*. The tx hashes of every transaction.

* **`tx_key_list`** - *array of: string*. The transaction keys for every transaction.

* **`amount_list`** - *array of: integer*. The amount transferred for every transaction.

* **`fee_list`** - *array of: integer*. The amount of fees paid for every transaction.

* **`tx_blob_list`** - *array of: string*. The tx as hex string for every transaction.

* **`tx_metadata_list`** - *array of: string*. List of transaction metadata needed to relay the transactions later.

* **`multisig_txset`** - *string*. The set of signing keys used in a multisig transaction (empty for non-multisig).

* **`unsigned_txset`** - *string*. Set of unsigned tx for cold-signing purposes.

!!! example
    **`sweep_all`**

      ```
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"sweep_all","params":{"address":"amitFhEesP4jSM3VEZxfz15sktBqxB3ioRtQ5QhVE1AZZnUJF47xALK7KMvhpJjvZN8QEZY2exCBx3NcVGNswec583K7dR1cnX","subaddr_indices":[4],"ring_size":7,"unlock_time":0,"get_tx_keys":true}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "amount_list": [9985885770000],
            "fee_list": [14114230000],
            "multisig_txset": "",
            "tx_hash_list": ["ab4b6b65cc8cd8c9dd317d0b90d97582d68d0aa1637b0065b05b61f9a66ea5c5"],
            "tx_key_list": ["b9b4b39d3bb3062ddb85ec0266d4df39058f4c86077d99309f218ce4d76af607"],
            "unsigned_txset": ""
          }
        }
      ```

---

---

###**`sweep_single`**

Send all of a specific unlocked output to an address.

**Inputs:**

* **`address`** - *string*; Destination public address.

* **`account_index`** - *unsigned int*; Sweep transactions from this account.

* **`subaddr_indices`** - *array of unsigned int*; (Optional) Sweep from this set of subaddresses in the account.

* **`priority`** - *unsigned int*; (Optional) Priority for sending the sweep transfer, partially determines fee.

* **`mixin`** - *unsigned int*; Number of outputs from the blockchain to mix with (0 means no mixing).

* **`unlock_time`** - *unsigned int*; Number of blocks before the amity can be spent (0 to not add a lock).

* **`payment_id`** - *string*; (Optional) Random 32-byte/64-character hex string to identify a transaction.

* **`get_tx_keys`** - **boolean**; (Optional) Return the transaction keys after sending.

* **`key_image`** - *string*; Key image of specific output to sweep.

* **`below_amount`** - *unsigned int*; (Optional) Include outputs below this amount.

* **`do_not_relay`** - **boolean**; (Optional) If true, do not relay this sweep transfer. (Defaults to false)

* **`get_tx_hex`** - **boolean**; (Optional) return the transactions as hex encoded string. (Defaults to false)

* **`get_tx_metadata`** - **boolean**; (Optional) return the transaction metadata as a string. (Defaults to false)

Outputs:

* **`tx_hash_list`** - *array of: string*. The tx hashes of every transaction.

* **`tx_key_list`** - *array of: string*. The transaction keys for every transaction.

* **`amount_list`** - *array of: integer*. The amount transferred for every transaction.

* **`fee_list`** - *array of: integer*. The amount of fees paid for every transaction.

* **`tx_blob_list`** - *array of: string*. The tx as hex string for every transaction.

* **`tx_metadata_list`** - *array of: string*. List of transaction metadata needed to relay the transactions later.

* **`multisig_txset`** - *string*. The set of signing keys used in a multisig transaction (empty for non-multisig).

* **`unsigned_txset`** - *string*. Set of unsigned tx for cold-signing purposes.

!!! example
    **`sweep_single`**
        
      ```  
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"sweep_single","params":{"address":"amitGvbEpdH6vcmWR3qXd9XXyud78DQfM84D2P9NMpJEV9iZFTTrmP5f6kUwWVmfNBXyMs4AufNUoUrGsqwfUn1L695cS6EkwC","unlock_time":0,"key_image":"a78344665d2fd75d8d4c712a6f8c8d928898968f8023f59ef795d2efb7804a5e","get_tx_keys":true}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "amount": 27126892247503,
            "fee": 14111630000,
            "multisig_txset": "",
            "tx_blob": "",
            "tx_hash": "106d434fc7edcfd91a0362fe4e5fabe4611e5b735c63233e3ded5558c275ae5b",
            "tx_key": "",
            "tx_metadata": "",
            "unsigned_txset": ""
          } 
        }
      ```  

---

---

###**`relay_tx`**

Relay a transaction previously created with `"do_not_relay":true`.

**Inputs:**

* **`hex`** - *string*; transaction metadata returned from a `transfer` method with `get_tx_metadata` set to `true`.

**Outputs:**

* **`tx_hash`** - *string*; for the publically searchable transaction hash.

!!! example
    **`relay_tx`**
        
      ```  
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"relay_tx","params":{"hex":"...tx_metadata..."}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "tx_hash": "1bccf33fbc5672bb091ef59a6dbd33b3d0cfb289c42dcab4a498a9b9e0e25fa5"
          }
        }
      ```  

---

---

###**`store`**

Save the wallet file.

**Inputs:** 

* *None*

**Outputs:** 

* *None*

!!! example
    **`store`**

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"store"}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
          }
        }
      ```
---

---
###**`get_payments`**

Get a list of incoming payments using a given payment id.

**Inputs:**

* **`payment_id`** - *string*; Payment ID used to find the payments (16 characters hex).

**Outputs:**

* **`payments`** - list of:

* **`payment_id`** - *string*; Payment ID matching the input parameter.

* **`tx_hash`** - *string*; Transaction hash used as the transaction ID.

* **`amount`** - *unsigned int*; Amount for this payment.

* **`block_height`** - *unsigned int*; Height of the block that first confirmed this payment.

* **`unlock_time`** - *unsigned int*; Time (in block height) until this payment is safe to spend.

* **`subaddr_index`** - subaddress index:

    * **`major`** - *unsigned int*; Account index for the subaddress.

    * **`minor`** - *unsigned int*; Index of the subaddress in the account.

    * **`address`** - *string*; Address receiving the payment; Base58 representation of the public keys.

!!! example
    **`get_payments`**

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_payments","params":{"payment_id":"60900e5603bf96e3"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "payments": [{
              "address": "amitFhEesP4jSM3VEZxfz15sktBqxB3ioRtQ5QhVE1AZZnUJF47xALK7KMvhpJjvZN8QEZY2exCBx3NcVGNswec583K7dR1cnX",
              "amount": 1000000000000,
              "block_height": 127606,
              "payment_id": "60900e5603bf96e3",
              "subaddr_index": {
                "major": 0,
                "minor": 0
              },
              "tx_hash": "3292e83ad28fc1cc7bc26dbd38862308f4588680fbf93eae3e803cddd1bd614f",
              "unlock_time": 0
            }]
          }
        }
      ```
---

---

###**`get_bulk_payments`**

Get a list of incoming payments using a given payment id, or a list of payments ids, from a given height. This method is the preferred method over get_payments because it has the same functionality but is more extendable. Either is fine for looking up transactions by a single payment ID.

**Inputs:**

* **`payment_ids`** - array of: *string*; Payment IDs used to find the payments (16 characters hex).

* **`min_block_height`** - *unsigned int*; The block height at which to start looking for payments.

**Outputs:**

* **`payments`** - list of:

* **`payment_id`** - *string*; Payment ID matching one of the input IDs.

* **`tx_hash`** - *string*; Transaction hash used as the transaction ID.

* **`amount`** - *unsigned int*; Amount for this payment.

* **`block_height`** - *unsigned int*; Height of the block that first confirmed this payment.

* **`unlock_time`** - *unsigned int*; Time (in block height) until this payment is safe to spend.

* **`subaddr_index`** - subaddress index:

* **`major`** - *unsigned int*; Account index for the subaddress.

* **`minor`** - *unsigned int*; Index of the subaddress in the account.

* **`address`** - *string*; Address receiving the payment; Base58 representation of the public keys.

!!! example
    **`get_bulk_payments`**

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_bulk_payments","params":{"payment_ids":["60900e5603bf96e3"],"min_block_height":"120000"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "payments": [{
              "address": "amitFhEesP4jSM3VEZxfz15sktBqxB3ioRtQ5QhVE1AZZnUJF47xALK7KMvhpJjvZN8QEZY2exCBx3NcVGNswec583K7dR1cnX",
              "amount": 1000000000000,
              "block_height": 127606,
              "payment_id": "60900e5603bf96e3",
              "subaddr_index": {
                "major": 0,
                "minor": 0
              },
              "tx_hash": "3292e83ad28fc1cc7bc26dbd38862308f4588680fbf93eae3e803cddd1bd614f",
              "unlock_time": 0
            }]
          }
        }
      ```      

---

---

###**`incoming_transfers`**

Return a list of incoming transfers to the wallet.

**Inputs:**

* **`transfer_type`** - *string*; "all": all the transfers, "available": only transfers which are not yet spent, OR "unavailable": only transfers which are already spent.

* **`account_index`** - *unsigned int*; (Optional) Return transfers for this account. (defaults to 0)

* **`subaddr_indices`** - array of *unsigned int*; (Optional) Return transfers sent to these subaddresses.

* **`verbose`** - *boolean*; (Optional) Enable verbose output, return key image if true.

**Outputs:**

* **`transfers`** - list of:

* **`amount`** - *unsigned int*; Amount of this transfer.

* **`global_index`** - *unsigned int*; Mostly internal use, can be ignored by most users.

* **`key_image`** - *string*; Key image for the incoming transfer's unspent output (empty unless verbose is true).

* **`spent`** - *boolean*; Indicates if this transfer has been spent.

* **`subaddr_index`** - *unsigned int*; Subaddress index for incoming transfer.

* **`tx_hash`** - *string*; Several incoming transfers may share the same hash if they were in the same transaction.

* **`tx_size`** - *unsigned int*; Size of transaction in bytes.

!!! example
    **`incoming_transfers`**

    Example, get all transfers:

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"incoming_transfers","params":{"transfer_type":"all","account_index":0,"subaddr_indices":[3],"verbose":true}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "transfers": [{
              "amount": 60000000000000,
              "global_index": 122405,
              "key_image": "768f5144777eb23477ab7acf83562581d690abaf98ca897c03a9d2b900eb479b",
              "spent": true,
              "subaddr_index": 3,
              "tx_hash": "f53401f21c6a43e44d5dd7a90eba5cf580012ad0e15d050059136f8a0da34f6b",
              "tx_size": 159
            },{
              "amount": 27126892247503,
              "global_index": 594994,
              "key_image": "7e561394806afd1be61980cc3431f6ef3569fa9151cd8d234f8ec13aa145695e",
              "spent": false,
              "subaddr_index": 3,
              "tx_hash": "106d4391a031e5b735ded555862fec63233e34e5fa4fc7edcfdbe461c275ae5b",
              "tx_size": 157
            },{
              "amount": 27169374733655,
              "global_index": 594997,
              "key_image": "e76c0a3bfeaae35e4173712f782eb34011198e26b990225b71aa787c8ba8a157",
              "spent": false,
              "subaddr_index": 3,
              "tx_hash": "0bd959b59117ee1254bd8e5aa8e77ec04ef744144a1ffb2d5c1eb9380a719621",
              "tx_size": 158
            }]
          }
        }
      ```

    Example, get available transfers:

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"incoming_transfers","params":{"transfer_type":"available","account_index":0,"subaddr_indices":[3],"verbose":true}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "transfers": [{
              "amount": 27126892247503,
              "global_index": 594994,
              "key_image": "7e561394806afd1be61980cc3431f6ef3569fa9151cd8d234f8ec13aa145695e",
              "spent": false,
              "subaddr_index": 3,
              "tx_hash": "106d4391a031e5b735ded555862fec63233e34e5fa4fc7edcfdbe461c275ae5b",
              "tx_size": 157
            },{
              "amount": 27169374733655,
              "global_index": 594997,
              "key_image": "e76c0a3bfeaae35e4173712f782eb34011198e26b990225b71aa787c8ba8a157",
              "spent": false,
              "subaddr_index": 3,
              "tx_hash": "0bd959b59117ee1254bd8e5aa8e77ec04ef744144a1ffb2d5c1eb9380a719621",
              "tx_size": 158
            }]
          }
        }
      ```

    Example, get unavailable transfers:

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"incoming_transfers","params":{"transfer_type":"unavailable","account_index":0,"subaddr_indices":[3],"verbose":true}}' -H 'Content-Type: application/json'
        {
        "id": "0",
        "jsonrpc": "2.0",
        "result": {
          "transfers": [{
            "amount": 60000000000000,
            "global_index": 122405,
            "key_image": "768f5144777eb23477ab7acf83562581d690abaf98ca897c03a9d2b900eb479b",
            "spent": true,
            "subaddr_index": 3,
            "tx_hash": "f53401f21c6a43e44d5dd7a90eba5cf580012ad0e15d050059136f8a0da34f6b",
            "tx_size": 159
          }]
        }
        }
      ```  

---

---
###**`query_key`**

Return the spend or view private key.

**Inputs:**

* **`key_type`** - *string*; Which key to retrieve: "mnemonic" - the mnemonic seed (older wallets do not have one) OR "view_key" - the view key

**Outputs:**

* **`key`** - *string*; The view key will be hex encoded, while the mnemonic will be a string of words.

!!! example
    **`query_key`**

    Example (Query view key):

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"query_key","params":{"key_type":"view_key"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "key": "0a1a38f6d246e894600a3e27238a064bf5e8d91801df47a17107596b1378e501"
          }
        }
      ```

    Example (Query mnemonic key):

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"query_key","params":{"key_type":"mnemonic"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "key": "vocal either anvil films dolphin zeal bacon cuisine quote syndrome rejoices envy okay pancakes tulips lair greater petals organs enmity dedicated oust thwart tomorrow tomorrow"
          }
        }
      ```
---

---

###**`make_integrated_address`**

Make an integrated address from the wallet address and a payment id.

**Inputs:**

* **`standard_address`** - *string*; (Optional, defaults to primary address) Destination public address.

* **`payment_id`** - *string*; (Optional, defaults to a random ID) 16 characters hex encoded.

**Outputs:**

* **`integrated_address`** - string

* **`payment_id`** - *string*; hex encoded;

!!! example
    **`make_integrated_address`**

    In the example shown the Payment ID is empty and will use a random ID:

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"make_integrated_address","params":{"standard_address":"amitFhEesP4jSM3VEZxfz15sktBqxB3ioRtQ5QhVE1AZZnUJF47xALK7KMvhpJjvZN8QEZY2exCBx3NcVGNswec583K7dR1cnX"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "integrated_address": "intARw9HKeaLQGJSPtbYDacR7dz8RBFnsfAKMaMuwUNYX6aQbBcovzDPyrQF9KXF9tVU6Xk3K8no1BywnJX6GvZXCkbHUXdPHyiUeRyokn",
            "payment_id": "420fa29b2d9a49f5"
          }
        }
      ```
---

---

###**`split_integrated_address`**

Retrieve the standard address and payment id corresponding to an integrated address.

**Inputs:**

* **`integrated_address`** - string

**Outputs:**

* **`is_subaddress`** - *boolean*; States if the address is a subaddress

* **`payment`** - *string*; hex encoded

* **`standard_address`** - string

!!! example
    **`split_integrated_address`**

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"split_integrated_address","params":{"integrated_address": "5F38Rw9HKeaLQGJSPtbYDacR7dz8RBFnsfAKMaMuwUNYX6aQbBcovzDPyrQF9KXF9tVU6Xk3K8no1BywnJX6GvZXCkbHUXdPHyiUeRyokn"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "is_subaddress": false,
            "payment_id": "420fa29b2d9a49f5",
            "standard_address": "amitFhEesP4jSM3VEZxfz15sktBqxB3ioRtQ5QhVE1AZZnUJF47xALK7KMvhpJjvZN8QEZY2exCBx3NcVGNswec583K7dR1cnX"
          }
        }
      ```
---

---

###**`stop_wallet`**

Stops the wallet, storing the current state.

**Inputs:**

* *None*

**Outputs:** 

* *None*

!!! example
    **`stop_wallet`**

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"stop_wallet"}' -H 'Content-Type: application/json'
        {    
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
          }
        }
      ```
---

---

###**`rescan_blockchain`**

Rescan the blockchain from scratch, losing any information which can not be recovered from the blockchain itself.
This includes destination addresses, tx secret keys, tx notes, etc.

**Inputs:**

* *None*

**Outputs:** 

* *None*

!!! example
    **`rescan_blockchain`**

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"rescan_blockchain"}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
          }
        }
      ```
---

---

###**`set_tx_notes`**

Set arbitrary string notes for transactions.

**Inputs:**

* **`txids`** - array of *string*; transaction ids

* **`notes`** - array of *string*; notes for the transactions

**Outputs:** 

* *None*

!!! example
    **`set_tx_notes`**

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"set_tx_notes","params":{"txids":["3292e83ad28fc1cc7bc26dbd38862308f4588680fbf93eae3e803cddd1bd614f"],"notes":["This is an example"]}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
          }
        }
      ```
---

---

###**`get_tx_notes`**

Get string notes for transactions.

**Inputs:**

* **`txids`** - array of *string*; transaction ids

**Outputs:**

* **`notes`** - array of *string*; notes for the transactions

!!! example
    **`get_tx_notes`**

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_tx_notes","params":{"txids":["3292e83ad28fc1cc7bc26dbd38862308f4588680fbf93eae3e803cddd1bd614f"]}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "notes": ["This is an example"]
          }
        }
      ```
---

---

###**`set_attribute`**

Set arbitrary attribute.

**Inputs:**

* **`key`** - *string*; attribute name

* **`value`** - *string*; attribute value

**Outputs:** 

* *None*

!!! example
    **`set_attribute`**

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"set_attribute","params":{"key":"my_attribute","value":"my_value"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
          }
        }
      ```
---

---

###**`get_attribute`**

Get attribute value by name.

**Inputs:**

* **`key`** - *string*; attribute name

**Outputs:**

* **`value`** - *string*; attribute value

!!! example
    **`get_attribute`**

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_attribute","params":{"key":"my_attribute"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "value": "my_value"
          }
        }
      ```
---

---

###**`get_tx_key`**

Get transaction secret key from transaction id.

**Inputs:**

* **`txid`** - *string*; transaction id.

**Outputs:**

* **`tx_key`** - *string*; transaction secret key.

!!! example
    **`get_tx_key`**
     
      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_tx_key","params":{"txid":"19d5089f9469db3d90aca9024dfcb17ce94b948300101c8345a5e9f7257353be"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "tx_key": "feba662cf8fb6d0d0da18fc9b70ab28e01cc76311278fdd7fe7ab16360762b06"
          }
        }
      ```
---

---

###**`check_tx_key`**

Check a transaction in the blockchain with its secret key.

**Inputs:**

* **`txid`** - *string*; transaction id.

* **`tx_key`** - *string*; transaction secret key.

* **`address`** - *string*; destination public address of the transaction.

**Outputs:**

* **`confirmations`** - *unsigned int*; Number of block mined after the one with the transaction.

* **`in_pool`** - *boolean*; States if the transaction is still in pool or has been added to a block.

* **`received`** - *unsigned int*; Amount of the transaction.

!!! example:
    **`check_tx_key`**

      ```  
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"check_tx_key","params":{"txid":"19d5089f9469db3d90aca9024dfcb17ce94b948300101c8345a5e9f7257353be","tx_key":"feba662cf8fb6d0d0da18fc9b70ab28e01cc76311278fdd7fe7ab16360762b06","address":"7BnERTpvL5MbCLtj5n9No7J5oE5hHiB3tVCK5cjSvCsYWD2WRJLFuWeKTLiXo5QJqt2ZwUaLy2Vh1Ad51K7FNgqcHgjW85o"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "confirmations": 0,
            "in_pool": false,
            "received": 100000000
          }
        }
      ```
---

---

###**`get_tx_proof`**

Get transaction signature to prove it.

**Inputs:**

* **`txid`** - *string*; transaction id.

* **`address`** - *string*; destination public address of the transaction.

* **`message`** - *string*; (Optional) add a message to the signature to further authenticate the prooving process.

**Outputs:**

* **`signature`** - *string*; transaction signature.

!!! example
    **`get_tx_proof`**

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_tx_proof","params":{"txid":"19d5089f9469db3d90aca9024dfcb17ce94b948300101c8345a5e9f7257353be","address":"7BnERTpvL5MbCLtj5n9No7J5oE5hHiB3tVCK5cjSvCsYWD2WRJLFuWeKTLiXo5QJqt2ZwUaLy2Vh1Ad51K7FNgqcHgjW85o","message":"this is my transaction"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "signature": "InProofV13vqBCT6dpSAXkypZmSEMPGVnNRFDX2vscUYeVS4WnSVnV5BwLs31T9q6Etfj9Wts6tAxSAS4gkMeSYzzLS7Gt4vvCSQRh9niGJMUDJsB5hTzb2XJiCkUzWkkcjLFBBRVD5QZ"
          }
        }
      ```
---

---

###**`check_tx_proof`**

Prove a transaction by checking its signature.

**Inputs:**

* **`txid`** - *string*; transaction id.

* **`address`** - *string*; destination public address of the transaction.

* **`message`** - *string*; (Optional) Should be the same message used in get_tx_proof.

* **`signature`** - *string*; transaction signature to confirm.

**Outputs:**

* **`confirmations`** - *unsigned int*; Number of block mined after the one with the transaction.

* **`good`** - *boolean*; States if the inputs proves the transaction.

* **`in_pool`** - *boolean*; States if the transaction is still in pool or has been added to a block.

* **`received`** - *unsigned int*; Amount of the transaction.

!!! example
    **`check_tx_proof`**

    In the example below, the transaction has been proven:

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"check_tx_proof","params":{"txid":"19d5089f9469db3d90aca9024dfcb17ce94b948300101c8345a5e9f7257353be","address":"7BnERTpvL5MbCLtj5n9No7J5oE5hHiB3tVCK5cjSvCsYWD2WRJLFuWeKTLiXo5QJqt2ZwUaLy2Vh1Ad51K7FNgqcHgjW85o","message":"this is my transaction","signature":"InProofV13vqBCT6dpSAXkypZmSEMPGVnNRFDX2vscUYeVS4WnSVnV5BwLs31T9q6Etfj9Wts6tAxSAS4gkMeSYzzLS7Gt4vvCSQRh9niGJMUDJsB5hTzb2XJiCkUzWkkcjLFBBRVD5QZ"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "confirmations": 482,
            "good": true,
            "in_pool": false,
            "received": 1000000000000
          }
        }
      ```

    In the example below, the wrong message is used, avoiding the transaction to be proved:

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"check_tx_proof","params":{"txid":"19d5089f9469db3d90aca9024dfcb17ce94b948300101c8345a5e9f7257353be","address":"7BnERTpvL5MbCLtj5n9No7J5oE5hHiB3tVCK5cjSvCsYWD2WRJLFuWeKTLiXo5QJqt2ZwUaLy2Vh1Ad51K7FNgqcHgjW85o","message":"wrong message","signature":"InProofV13vqBCT6dpSAXkypZmSEMPGVnNRFDX2vscUYeVS4WnSVnV5BwLs31T9q6Etfj9Wts6tAxSAS4gkMeSYzzLS7Gt4vvCSQRh9niGJMUDJsB5hTzb2XJiCkUzWkkcjLFBBRVD5QZ"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "confirmations": 0,
            "good": false,
            "in_pool": false,
            "received": 0
          }
        }
      ```  

---

---

###**`get_spend_proof`**

Generate a signature to prove a spend. Unlike proving a transaction, it does not requires the destination public address.

**Inputs:**

* **`txid`** - *string*; transaction id.

* **`message`** - *string*; (Optional) add a message to the signature to further authenticate the prooving process.

**Outputs:**

* **`signature`** - *string*; spend signature.

!!! example
    **`get_spend_proof`**

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_spend_proof","params":{"txid":"19d5089f9469db3d90aca9024dfcb17ce94b948300101c8345a5e9f7257353be","message":"this is my transaction"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "signature": "SpendProofV1aSh8Todhk54736iXgV6vJAFP7egxByuMWZeyNDaN2JY737S95X5zz5mNMQSuCNSLjjhi5HJCsndpNWSNVsuThxwv285qy1KkUrLFRkxMSCjfL6bbycYN33ScZ5UB4Fzseceo1ndpL393T1q638VmcU3a56dhNHF1RPZFiGPS61FA78nXFSqE9uoKCCoHkEz83M1dQVhxZV5CEPF2P6VioGTKgprLCH9vvj9k1ivd4SX19L2VSMc3zD1u3mkR24ioETvxBoLeBSpxMoikyZ6inhuPm8yYo9YWyFtQK4XYfAV9mJ9knz5fUPXR8vvh7KJCAg4dqeJXTVb4mbMzYtsSZXHd6ouWoyCd6qMALdW8pKhgMCHcVYMWp9X9WHZuCo9rsRjRpg15sJUw7oJg1JoGiVgj8P4JeGDjnZHnmLVa5bpJhVCbMhyM7JLXNQJzFWTGC27TQBbthxCfQaKdusYnvZnKPDJWSeceYEFzepUnsWhQtyhbb73FzqgWC4eKEFKAZJqT2LuuSoxmihJ9acnFK7Ze23KTVYgDyMKY61VXADxmSrBvwUtxCaW4nQtnbMxiPMNnDMzeixqsFMBtN72j5UqhiLRY99k6SE7Qf5f29haNSBNSXCFFHChPKNTwJrehkofBdKUhh2VGPqZDNoefWUwfudeu83t85bmjv8Q3LrQSkFgFjRT5tLo8TMawNXoZCrQpyZrEvnodMDDUUNf3NL7rxyv3gM1KrTWjYaWXFU2RAsFee2Q2MTwUW7hR25cJvSFuB1BX2bfkoCbiMk923tHZGU2g7rSKF1GDDkXAc1EvFFD4iGbh1Q5t6hPRhBV8PEncdcCWGq5uAL5D4Bjr6VXG8uNeCy5oYWNgbZ5JRSfm7QEhPv8Fy9AKMgmCxDGMF9dVEaU6tw2BAnJavQdfrxChbDBeQXzCbCfep6oei6n2LZdE5Q84wp7eoQFE5Cwuo23tHkbJCaw2njFi3WGBbA7uGZaGHJPyB2rofTWBiSUXZnP2hiE9bjJghAcDm1M4LVLfWvhZmFEnyeru3VWMETnetz1BYLUC5MJGFXuhnHwWh7F6r74FDyhdswYop4eWPbyrXMXmUQEccTGd2NaT8g2VHADZ76gMC6BjWESvcnz2D4n8XwdmM7ZQ1jFwhuXrBfrb1dwRasyXxxHMGAC2onatNiExyeQ9G1W5LwqNLAh9hvcaNTGaYKYXoceVzLkgm6e5WMkLsCwuZXvB"
          }
        }
      ```
---

---

###**`check_spend_proof`**

Prove a spend using a signature. Unlike proving a transaction, it does not requires the destination public address.

**Inputs:**

* **`txid`** - *string*; transaction id.

* **`message`** - *string*; (Optional) Should be the same message used in get_spend_proof.

* **`signature`** - *string*; spend signature to confirm.

**Outputs:**

* **`good`** - *boolean*; States if the inputs proves the spend.

!!! example
    **`check_spend_proof`**

    In the example below, the spend has been proven:

      ```  
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"check_spend_proof","params":{"txid":"19d5089f9469db3d90aca9024dfcb17ce94b948300101c8345a5e9f7257353be","message":"this is my transaction","signature":"SpendProofV1aSh8Todhk54736iXgV6vJAFP7egxByuMWZeyNDaN2JY737S95X5zz5mNMQSuCNSLjjhi5HJCsndpNWSNVsuThxwv285qy1KkUrLFRkxMSCjfL6bbycYN33ScZ5UB4Fzseceo1ndpL393T1q638VmcU3a56dhNHF1RPZFiGPS61FA78nXFSqE9uoKCCoHkEz83M1dQVhxZV5CEPF2P6VioGTKgprLCH9vvj9k1ivd4SX19L2VSMc3zD1u3mkR24ioETvxBoLeBSpxMoikyZ6inhuPm8yYo9YWyFtQK4XYfAV9mJ9knz5fUPXR8vvh7KJCAg4dqeJXTVb4mbMzYtsSZXHd6ouWoyCd6qMALdW8pKhgMCHcVYMWp9X9WHZuCo9rsRjRpg15sJUw7oJg1JoGiVgj8P4JeGDjnZHnmLVa5bpJhVCbMhyM7JLXNQJzFWTGC27TQBbthxCfQaKdusYnvZnKPDJWSeceYEFzepUnsWhQtyhbb73FzqgWC4eKEFKAZJqT2LuuSoxmihJ9acnFK7Ze23KTVYgDyMKY61VXADxmSrBvwUtxCaW4nQtnbMxiPMNnDMzeixqsFMBtN72j5UqhiLRY99k6SE7Qf5f29haNSBNSXCFFHChPKNTwJrehkofBdKUhh2VGPqZDNoefWUwfudeu83t85bmjv8Q3LrQSkFgFjRT5tLo8TMawNXoZCrQpyZrEvnodMDDUUNf3NL7rxyv3gM1KrTWjYaWXFU2RAsFee2Q2MTwUW7hR25cJvSFuB1BX2bfkoCbiMk923tHZGU2g7rSKF1GDDkXAc1EvFFD4iGbh1Q5t6hPRhBV8PEncdcCWGq5uAL5D4Bjr6VXG8uNeCy5oYWNgbZ5JRSfm7QEhPv8Fy9AKMgmCxDGMF9dVEaU6tw2BAnJavQdfrxChbDBeQXzCbCfep6oei6n2LZdE5Q84wp7eoQFE5Cwuo23tHkbJCaw2njFi3WGBbA7uGZaGHJPyB2rofTWBiSUXZnP2hiE9bjJghAcDm1M4LVLfWvhZmFEnyeru3VWMETnetz1BYLUC5MJGFXuhnHwWh7F6r74FDyhdswYop4eWPbyrXMXmUQEccTGd2NaT8g2VHADZ76gMC6BjWESvcnz2D4n8XwdmM7ZQ1jFwhuXrBfrb1dwRasyXxxHMGAC2onatNiExyeQ9G1W5LwqNLAh9hvcaNTGaYKYXoceVzLkgm6e5WMkLsCwuZXvB"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "good": true
          }
        }
      ```      

    In the example below, the wrong message is used, avoiding the spend to be proved:

        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"check_spend_proof","params":{"txid":"19d5089f9469db3d90aca9024dfcb17ce94b948300101c8345a5e9f7257353be","message":"wrong message","signature":"SpendProofV1aSh8Todhk54736iXgV6vJAFP7egxByuMWZeyNDaN2JY737S95X5zz5mNMQSuCNSLjjhi5HJCsndpNWSNVsuThxwv285qy1KkUrLFRkxMSCjfL6bbycYN33ScZ5UB4Fzseceo1ndpL393T1q638VmcU3a56dhNHF1RPZFiGPS61FA78nXFSqE9uoKCCoHkEz83M1dQVhxZV5CEPF2P6VioGTKgprLCH9vvj9k1ivd4SX19L2VSMc3zD1u3mkR24ioETvxBoLeBSpxMoikyZ6inhuPm8yYo9YWyFtQK4XYfAV9mJ9knz5fUPXR8vvh7KJCAg4dqeJXTVb4mbMzYtsSZXHd6ouWoyCd6qMALdW8pKhgMCHcVYMWp9X9WHZuCo9rsRjRpg15sJUw7oJg1JoGiVgj8P4JeGDjnZHnmLVa5bpJhVCbMhyM7JLXNQJzFWTGC27TQBbthxCfQaKdusYnvZnKPDJWSeceYEFzepUnsWhQtyhbb73FzqgWC4eKEFKAZJqT2LuuSoxmihJ9acnFK7Ze23KTVYgDyMKY61VXADxmSrBvwUtxCaW4nQtnbMxiPMNnDMzeixqsFMBtN72j5UqhiLRY99k6SE7Qf5f29haNSBNSXCFFHChPKNTwJrehkofBdKUhh2VGPqZDNoefWUwfudeu83t85bmjv8Q3LrQSkFgFjRT5tLo8TMawNXoZCrQpyZrEvnodMDDUUNf3NL7rxyv3gM1KrTWjYaWXFU2RAsFee2Q2MTwUW7hR25cJvSFuB1BX2bfkoCbiMk923tHZGU2g7rSKF1GDDkXAc1EvFFD4iGbh1Q5t6hPRhBV8PEncdcCWGq5uAL5D4Bjr6VXG8uNeCy5oYWNgbZ5JRSfm7QEhPv8Fy9AKMgmCxDGMF9dVEaU6tw2BAnJavQdfrxChbDBeQXzCbCfep6oei6n2LZdE5Q84wp7eoQFE5Cwuo23tHkbJCaw2njFi3WGBbA7uGZaGHJPyB2rofTWBiSUXZnP2hiE9bjJghAcDm1M4LVLfWvhZmFEnyeru3VWMETnetz1BYLUC5MJGFXuhnHwWh7F6r74FDyhdswYop4eWPbyrXMXmUQEccTGd2NaT8g2VHADZ76gMC6BjWESvcnz2D4n8XwdmM7ZQ1jFwhuXrBfrb1dwRasyXxxHMGAC2onatNiExyeQ9G1W5LwqNLAh9hvcaNTGaYKYXoceVzLkgm6e5WMkLsCwuZXvB"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "good": false
          }
        }
      ```
---

---

###**`get_reserve_proof`**

Generate a signature to prove of an available amount in a wallet.

**Inputs:**

* **`all`** - *boolean*; Proves all wallet balance to be disposable.

* **`account_index`** - *unsigned int*; Specify the account from witch to prove reserve. (ignored if all is set to true)

* **`amount`** - *unsigned int*; Amount (in atomic units) to prove the account has for reserve. (ignored if all is set to true)

* **`message`** - *string*; (Optional) add a message to the signature to further authenticate the prooving process.

**Outputs:**

* **`signature`** - *string*; reserve signature.

!!! example
    **`get_reserve_proof`**

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_reserve_proof","params":{"all":false,"account_index":0,"amount":100000000000}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "signature": "ReserveProofV11BZ23sBt9sZJeGccf84mzyAmNCP3KzYbE1111112VKmH111118NfCYJQjZ6c46gT2kXgcHCaSSZeL8sRdzqjqx7i1e7FQfQGu2o113UYFVdwzHQi3iENDPa76Kn1BvywbKz3bMkXdZkBEEhBSF4kjjGaiMJ1ucKb6wvMVC4A8sA4nZEdL2Mk3wBucJCYTZwKqA8i1M113kqakDkG25FrjiDqdQTCYz2wDBmfKxF3eQiV5FWzZ6HmAyxnqTWUiMWukP9A3Edy3ZXqjP1b23dhz7Mbj39bBxe3ZeDNu9HnTSqYvHNRyqCkeUMJpHyQweqjGUJ1DSfFYr33J1E7MkhMnEi1o7trqWjVix32XLetYfePG73yvHbS24837L7Q64i5n1LSpd9yMiQZ3Dyaysi5y6jPx7TpAvnSqBFtuCciKoNzaXoA3dqt9cuVFZTXzdXKqdt3cXcVJMNxY8RvKPVQHhUur94Lpo1nSpxf7BN5a5rHrbZFqoZszsZmiWikYPkLX72XUdw6NWjLrTBxSy7KuPYH86c6udPEXLo2xgN6XHMBMBJzt8FqqK7EcpNUBkuHm2AtpGkf9CABY3oSjDQoRF5n4vNLd3qUaxNsG4XJ12L9gJ7GrK273BxkfEA8fDdxPrb1gpespbgEnCTuZHqj1A"
          }
        }
      ```
---

---

###**`check_reserve_proof`**

Proves a wallet has a disposable reserve using a signature.

**Inputs:**

* **`address`** - *string*; Public address of the wallet.

* **`message`** - *string*; (Optional) Should be the same message used in get_reserve_proof.

* **`signature`** - *string*; reserve signature to confirm.

**Outputs:**

* **`good`** - *boolean*; States if the inputs proves the reserve.

!!! example
    **`check_reserve_proof`**

    In the example below, the reserve has been proven:

      ```    
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"check_reserve_proof","params":{"address":"amitFhEesP4jSM3VEZxfz15sktBqxB3ioRtQ5QhVE1AZZnUJF47xALK7KMvhpJjvZN8QEZY2exCBx3NcVGNswec583K7dR1cnX","signature":"ReserveProofV11BZ23sBt9sZJeGccf84mzyAmNCP3KzYbE1111112VKmH111118NfCYJQjZ6c46gT2kXgcHCaSSZeL8sRdzqjqx7i1e7FQfQGu2o113UYFVdwzHQi3iENDPa76Kn1BvywbKz3bMkXdZkBEEhBSF4kjjGaiMJ1ucKb6wvMVC4A8sA4nZEdL2Mk3wBucJCYTZwKqA8i1M113kqakDkG25FrjiDqdQTCYz2wDBmfKxF3eQiV5FWzZ6HmAyxnqTWUiMWukP9A3Edy3ZXqjP1b23dhz7Mbj39bBxe3ZeDNu9HnTSqYvHNRyqCkeUMJpHyQweqjGUJ1DSfFYr33J1E7MkhMnEi1o7trqWjVix32XLetYfePG73yvHbS24837L7Q64i5n1LSpd9yMiQZ3Dyaysi5y6jPx7TpAvnSqBFtuCciKoNzaXoA3dqt9cuVFZTXzdXKqdt3cXcVJMNxY8RvKPVQHhUur94Lpo1nSpxf7BN5a5rHrbZFqoZszsZmiWikYPkLX72XUdw6NWjLrTBxSy7KuPYH86c6udPEXLo2xgN6XHMBMBJzt8FqqK7EcpNUBkuHm2AtpGkf9CABY3oSjDQoRF5n4vNLd3qUaxNsG4XJ12L9gJ7GrK273BxkfEA8fDdxPrb1gpespbgEnCTuZHqj1A"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "good": true,
            "spent": 0,
            "total": 100000000000
          }
        }
      ```  
    
    In the example below, all wallet reserve has been proven:

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"check_reserve_proof","params":{"address":"amitFhEesP4jSM3VEZxfz15sktBqxB3ioRtQ5QhVE1AZZnUJF47xALK7KMvhpJjvZN8QEZY2exCBx3NcVGNswec583K7dR1cnX","message":"I have 10 at least","signature":"...signature..."}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "good": true,
            "spent": 0,
            "total": 164113855714662789
          }
        }
      ```
    In the example below, the wrong message is used, avoiding the reserve to be proved:

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"check_spend_proof","params":{"txid":"19d5089f9469db3d90aca9024dfcb17ce94b948300101c8345a5e9f7257353be","message":"wrong message","signature":"SpendProofV1aSh8Todhk54736iXgV6vJAFP7egxByuMWZeyNDaN2JY737S95X5zz5mNMQSuCNSLjjhi5HJCsndpNWSNVsuThxwv285qy1KkUrLFRkxMSCjfL6bbycYN33ScZ5UB4Fzseceo1ndpL393T1q638VmcU3a56dhNHF1RPZFiGPS61FA78nXFSqE9uoKCCoHkEz83M1dQVhxZV5CEPF2P6VioGTKgprLCH9vvj9k1ivd4SX19L2VSMc3zD1u3mkR24ioETvxBoLeBSpxMoikyZ6inhuPm8yYo9YWyFtQK4XYfAV9mJ9knz5fUPXR8vvh7KJCAg4dqeJXTVb4mbMzYtsSZXHd6ouWoyCd6qMALdW8pKhgMCHcVYMWp9X9WHZuCo9rsRjRpg15sJUw7oJg1JoGiVgj8P4JeGDjnZHnmLVa5bpJhVCbMhyM7JLXNQJzFWTGC27TQBbthxCfQaKdusYnvZnKPDJWSeceYEFzepUnsWhQtyhbb73FzqgWC4eKEFKAZJqT2LuuSoxmihJ9acnFK7Ze23KTVYgDyMKY61VXADxmSrBvwUtxCaW4nQtnbMxiPMNnDMzeixqsFMBtN72j5UqhiLRY99k6SE7Qf5f29haNSBNSXCFFHChPKNTwJrehkofBdKUhh2VGPqZDNoefWUwfudeu83t85bmjv8Q3LrQSkFgFjRT5tLo8TMawNXoZCrQpyZrEvnodMDDUUNf3NL7rxyv3gM1KrTWjYaWXFU2RAsFee2Q2MTwUW7hR25cJvSFuB1BX2bfkoCbiMk923tHZGU2g7rSKF1GDDkXAc1EvFFD4iGbh1Q5t6hPRhBV8PEncdcCWGq5uAL5D4Bjr6VXG8uNeCy5oYWNgbZ5JRSfm7QEhPv8Fy9AKMgmCxDGMF9dVEaU6tw2BAnJavQdfrxChbDBeQXzCbCfep6oei6n2LZdE5Q84wp7eoQFE5Cwuo23tHkbJCaw2njFi3WGBbA7uGZaGHJPyB2rofTWBiSUXZnP2hiE9bjJghAcDm1M4LVLfWvhZmFEnyeru3VWMETnetz1BYLUC5MJGFXuhnHwWh7F6r74FDyhdswYop4eWPbyrXMXmUQEccTGd2NaT8g2VHADZ76gMC6BjWESvcnz2D4n8XwdmM7ZQ1jFwhuXrBfrb1dwRasyXxxHMGAC2onatNiExyeQ9G1W5LwqNLAh9hvcaNTGaYKYXoceVzLkgm6e5WMkLsCwuZXvB"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "good": false
          }
        }
      ```
---

---

###**`get_transfers`**

Returns a list of transfers.

**Inputs:**

* **`in`** - *boolean*; (Optional) Include incoming transfers.

* **`out`** - *boolean*; (Optional) Include outgoing transfers.

* **`pending`** - *boolean*; (Optional) Include pending transfers.

* **`failed`** - *boolean*; (Optional) Include failed transfers.

* **`pool`** - *boolean*; (Optional) Include transfers from the daemon's transaction pool.

* **`filter_by_height`** - *boolean*; (Optional) Filter transfers by block height.

* **`min_height`** - *unsigned int*; (Optional) Minimum block height to scan for transfers, if filtering by height is enabled.

* **`max_height`** - *unsigned int*; (Opional) Maximum block height to scan for transfers, if filtering by height is enabled (defaults to max block height).

* **`account_index`** - *unsigned int*; (Optional) Index of the account to query for transfers. (defaults to 0)

* **`subaddr_indices`** - *array of unsigned int*; (Optional) List of subaddress indices to query for transfers. (Defaults to empty - all indices)

**Outputs:**

in array of transfers:

* **`address`** - *string*; Public address of the transfer.

* **`amount`** - *unsigned int*; Amount transferred.

* **`confirmations`** - *unsigned int*; Number of block mined since the block containing this transaction (or block height at which the transaction should be added to a block if not yet confirmed).

* **`double_spend_seen`** - **boolean**; True if the key image(s) for the transfer have been seen before.

* **`fee`** - *unsigned int*; Transaction fee for this transfer.

* **`height`** - *unsigned int*; Height of the first block that confirmed this transfer (0 if not mined yet).

* **`note`** - *string*; Note about this transfer.

* **`payment_id`** - *string*; Payment ID for this transfer.

* **`subaddr_index`** - JSON object containing the major & minor subaddress index:

* **`major`** - *unsigned int*; Account index for the subaddress.

* **`minor`** - *unsigned int*; Index of the subaddress under the account.

* **`suggested_confirmations_threshold`** - *unsigned int*; Estimation of the confirmations needed for the transaction to be included in a block.

* **`timestamp`** - *unsigned int*; POSIX timestamp for when this transfer was first confirmed in a block (or timestamp submission if not mined yet.

* **`txid`** - *string*; Transaction ID for this transfer.

* **`type`** - *string*; Transfer type: "in"

* **`unlock_time`** - *unsigned int*; Number of blocks until transfer is safely spendable.

* **out array of transfers** (see above).

* **pending array of transfers** (see above).

* **failed array of transfers** (see above).

* **pool array of transfers** (see above).

!!! example
    **`get_transfers`**


      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_transfers","params":{"in":true,"account_index":1}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "in": [{
              "address": "amitFhEesP4MvhpJjvZN8QEZY2exCBx3NcVGNswec583K7dR1cnXjSM3VEZxfz15sktBqxB3ioRtQ5QhVE1AZZnUJF47xALK7K",
              "amount": 200000000000,
              "confirmations": 1,
              "double_spend_seen": false,
              "fee": 10000,
              "height": 153624,
              "note": "",
              "payment_id": "0000000000000000",
              "subaddr_index": {
                "major": 1,
                "minor": 0
              },
              "suggested_confirmations_threshold": 1,
              "timestamp": 1535918400,
              "txid": "c36258a276018c3a4bc1f195a7fb530f50cd63a4fa765fb7c6f7f49fc051762a",
              "type": "in",
              "unlock_time": 0
            }]
          }
        }
      ```
---

---

###**`get_transfer_by_txid`**

Show information about a transfer to/from this address.

**Inputs:**

* **`txid`** - *string*; Transaction ID used to find the transfer.

* **`account_index`** - *unsigned int*; (Optional) Index of the account to query for the transfer.

**Outputs:**

* **`transfer`** - JSON object containing payment information:

* **`address`** - *string*; Address that transferred the funds. Base58 representation of the public keys.

* **`amount`** - *unsigned int*; Amount of this transfer.

* **`confirmations`** - *unsigned int*; Number of block mined since the block containing this transaction (or block height at which the transaction should be added to a block if not yet confirmed).

* **`destinations`** - array of JSON objects containing transfer destinations:

* **`amount`** - *unsigned int*; Amount transferred to this destination.

* **`address`** - *string*; Address for this destination. Base58 representation of the public keys.

* **`double_spend_seen`** - **boolean**; True if the key image(s) for the transfer have been seen before.

* **`fee`** - *unsigned int*; Transaction fee for this transfer.

* **`height`** - *unsigned int*; Height of the first block that confirmed this transfer.

* **`note`** - *string*; Note about this transfer.

* **`payment_id`** - *string*; Payment ID for this transfer.

* **`ubaddr_index`** - JSON object containing the major & minor subaddress index:

* **`major`** - *unsigned int*; Account index for the subaddress.

* **`minor`** - *unsigned int*; Index of the subaddress under the account.

* **`suggested_confirmations_threshold`** - *unsigned int*; Estimation of the confirmations needed for the transaction to be included in a block.

* **`timestamp`** - *unsigned int*; POSIX timestamp for the block that confirmed this transfer (or timestamp submission if not mined yet).

* **`txid`** - *string*; Transaction ID of this transfer (same as input TXID).

* **`type`** - *string*; Type of transfer, one of the following: "in", "out", "pending", "failed", "pool"

* **`unlock_time`** - *unsigned int*; Number of blocks until transfer is safely spendable.

!!! example
    **`get_transfer_by_txid`**

      ```
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_transfer_by_txid","params":{"txid":"c36258a276018c3a4bc1f195a7fb530f50cd63a4fa765fb7c6f7f49fc051762a"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "transfer": {
              "address": "amitFhEesP4jSM3VEZxfz15sktBqxB3ioRtQ5QhVE1AZZnUJF47xALK7KMvhpJjvZN8QEZY2exCBx3NcVGNswec583K7dR1cnX",
              "amount": 300000000000,
              "confirmations": 1,
              "destinations": [{
                "address": "amitFhEesP4jktBqxB3ioRtQ5QhVE1AZZnUJVGNswec583K7dR1cnF47xALK7KMvhpJjvZN8QEZY2exCBx3NcXSM3VEZxfz15s",
                "amount": 100000000000
              },{
                "address": "amitFhEesP4MvhpJjvZN8QEZY2exCBx3NcVGNswec583K7dR1cnXjSM3VEZxfz15sktBqxB3ioRtQ5QhVE1AZZnUJF47xALK7K",
                "amount": 200000000000
              }],
              "double_spend_seen": false,
              "fee": 10000,
              "height": 15362,
              "note": "",
              "payment_id": "0000000000000000",
              "subaddr_index": {
                "major": 0,
                "minor": 0
              },
              "suggested_confirmations_threshold": 1,
              "timestamp": 1535918400,
              "txid": "c36258a276018c3a4bc1f195a7fb530f50cd63a4fa765fb7c6f7f49fc051762a",
              "type": "out",
              "unlock_time": 0
            }
          }
        }
      ```

---

---

###**`sign`**

Sign a string.

**Inputs:**

* **`data`** - *string*; Anything you need to sign.

**Outputs:**

* **`signature`** - *string*; Signature generated against the "data" and the account public address.

!!! example
    **`sign`**

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"sign","params":{"data":"This is sample data to be signed"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "signature": "SigV14K6G151gycjiGxjQ74tKX6A2LwwghvuHjcDeuRFQio5LS6Gb27BNxjYQY1dPuUvXkEbGQUkiHSVLPj4nJAHRrrw3"
          }
        }
      ```
---

---

###**`verify`**

Verify a signature on a string.

**Inputs:**

* **`data`** - *string*; What should have been signed.

* **`address`** - *string*; Public address of the wallet used to sign the data.

* **`signature`** - *string*; signature generated by sign method.

**Outputs:**

* **`good`** - *boolean*;

!!! example
    **`verify`**

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"verify","params":{"data":"This is sample data to be signed","address":"amitFhEesP4jSM3VEZxfz15sktBqxB3ioRtQ5QhVE1AZZnUJF47xALK7KMvhpJjvZN8QEZY2exCBx3NcVGNswec583K7dR1cnX","signature":"SigV14K6G151gycjiGxjQ74tKX6A2LwwghvuHjcDeuRFQio5LS6Gb27BNxjYQY1dPuUvXkEbGQUkiHSVLPj4nJAHRrrw3"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "good": true
          }
        }
      ```
---

---

###**`export_outputs`**

Export all outputs in hex format.

**Inputs:** 

* *None*

**Outputs:**

* **`outputs_data_hex`** - *string*; wallet outputs in hex format.

!!! example
    **`export_outputs`**

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"export_outputs"}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "outputs_data_hex": "...outputs..."
          }
        }
      ```
---

---

###**`import_outputs`**

Import outputs in hex format.

**Inputs:**

* **`outputs_data_hex`** - *string*; wallet outputs in hex format.

**Outputs:**

* **`num_imported`** - *unsigned int*; number of outputs imported.

!!! example
    **`import_outputs`**

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"import_outputs","params":{"outputs_data_hex":"...outputs..."}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "num_imported": 6400
          }
        }
      ```
---

---

###**`export_key_images`**

Export a signed set of key images.

**Inputs:** 

* *None*

**Outputs:**

* **`signed_key_images`** - array of signed key images:

* **`key_image`** - *string*;

* **`signature`** - *string*;

!!! example
    **`export_key_images`**

      ```
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"export_key_images"}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "signed_key_images": [{
              "key_image": "cd35239b72a35e26a57ed17400c0b66944a55de9d5bda0f21190fed17f8ea876",
              "signature": "c9d736869355da2538ab4af188279f84138c958edbae3c5caf388a63cd8e780b8c5a1aed850bd79657df659422c463608ea4e0c730ba9b662c906ae933816d00"
            },{
              "key_image": "65158a8ee5a3b32009b85a307d85b375175870e560e08de313531c7dbbe6fc19",
              "signature": "c96e40d09dfc45cfc5ed0b76bfd7ca793469588bb0cf2b4d7b45ef23d40fd4036057b397828062e31700dc0c2da364f50cd142295a8405b9fe97418b4b745d0c"
            },...]
          }
        }
      ```
---

---

###**`import_key_images`**

Import signed key images list and verify their spent status.

**Inputs:**

* **`signed_key_images`** - array of signed key images:

* **`key_image`** - *string*;

* **`signature`** - *string*;

**Outputs:**

* **`height`** - *unsigned int*;

* **`spent`** - *unsigned int*; Amount (in atomic units) spent from those key images.

* **`unspent`** - *unsigned int*; Amount (in atomic units) still available from those key images.

!!! example
    **`import_key_images`**

      ```
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"import_key_images", "params":{"signed_key_images":[{"key_image":"cd35239b72a35e26a57ed17400c0b66944a55de9d5bda0f21190fed17f8ea876","signature":"c9d736869355da2538ab4af188279f84138c958edbae3c5caf388a63cd8e780b8c5a1aed850bd79657df659422c463608ea4e0c730ba9b662c906ae933816d00"},{"key_image":"65158a8ee5a3b32009b85a307d85b375175870e560e08de313531c7dbbe6fc19","signature":"c96e40d09dfc45cfc5ed0b76bfd7ca793469588bb0cf2b4d7b45ef23d40fd4036057b397828062e31700dc0c2da364f50cd142295a8405b9fe97418b4b745d0c"}]}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "height": 76428,
            "spent": 62708953408711,
            "unspent": 0
          }
        }
      ```
---

---

###**`make_uri`**

Create a payment URI using the official URI spec.

**Inputs:**

* **`address`** - *string*; Wallet address

* **`amount`** - *unsigned int*; (optional) the integer amount to receive, in atomic units

* **`payment_id`** - *string*; (optional) 16 or 64 character hexadecimal payment id

* **`recipient_name`** - *string*; (optional) name of the payment recipient

* **`tx_description`** - *string*; (optional) Description of the reason for the tx

**Outputs:**

* **`uri`** - *string*; This contains all the payment input information as a properly formatted payment URI

!!! example
    **`make_uri`**

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"make_uri","params":{"address":"amitFhEesP4jSM3VEZxfz15sktBqxB3ioRtQ5QhVE1AZZnUJF47xALK7KMvhpJjvZN8QEZY2exCBx3NcVGNswec583K7dR1cnX","amount":10,"payment_id":"420fa29b2d9a49f5","tx_description":"Testing out the make_uri function.","recipient_name":"el00ruobuob Stagenet wallet"}}'  -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "uri": "amity:amitFhEesP4jSM3VEZxfz15sktBqxB3ioRtQ5QhVE1AZZnUJF47xALK7KMvhpJjvZN8QEZY2exCBx3NcVGNswec583K7dR1cnX?tx_payment_id=420fa29b2d9a49f5&tx_amount=0.000000000010&recipient_name=el00ruobuob%20Stagenet%20wallet&tx_description=Testing%20out%20the%20make_uri%20function."
          }
        }
      ```
---

---

###**`parse_uri`**

Parse a payment URI to get payment information.

**Inputs:**

* **`uri`** - *string*; This contains all the payment input information as a properly formatted payment URI

**Outputs:**

* **`uri`** - JSON object containing payment information:

* **`address` - *string*; Wallet address

* **`amount`** - *unsigned int*; Integer amount to receive, in atomic units (0 if not provided)

* **`payment_id`** - *string*; 16 or 64 character hexadecimal payment id (empty if not provided)

* **`recipient_name`** - *string*; Name of the payment recipient (empty if not provided)

* **`tx_description`** - *string*; Description of the reason for the tx (empty if not provided)

!!! example
    **`parse_uri`**

      ```
        $ curl -X POST http://127.0.0.1:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"parse_uri","params":{"uri":"amity:amitFhEesP4jSM3VEZxfz15sktBqxB3ioRtQ5QhVE1AZZnUJF47xALK7KMvhpJjvZN8QEZY2exCBx3NcVGNswec583K7dR1cnX?tx_payment_id=420fa29b2d9a49f5&tx_amount=0.000000000010&recipient_name=el00ruobuob%20Stagenet%20wallet&tx_description=Testing%20out%20the%20make_uri%20function."}}' -H 'Content-Type: application/json'
        { 
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "uri": {
              "address": "amitFhEesP4jSM3VEZxfz15sktBqxB3ioRtQ5QhVE1AZZnUJF47xALK7KMvhpJjvZN8QEZY2exCBx3NcVGNswec583K7dR1cnX",
              "amount": 10,
              "payment_id": "420fa29b2d9a49f5",
              "recipient_name": "el00ruobuob Stagenet wallet",
              "tx_description": "Testing out the make_uri function."
            }
          }
        }
      ```  

---

---

###**`get_address_book`**

Retrieves entries from the address book.

**Inputs:**

* **`entries`** - *array of unsigned int*; indices of the requested address book entries

**Outputs:**

* **`entries`** - array of entries:

* **`address`** - *string*; Public address of the entry

* **`description`** - *string*; Description of this address entry

* **`index`** - *unsigned int*;

* **`payment_id`** - *string*;

!!! example
    **`get_address_book`**

      ```
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_address_book","params":{"entries":[0,1]}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "entries": [{
              "address": "amitFhEesP4MvhpJjvZN8QEZY2exCBx3NcVGNswec583K7dR1cnXjSM3VEZxfz15sktBqxB3ioRtQ5QhVE1AZZnUJF47xALK7K",
              "description": "Second account",
              "index": 0,
              "payment_id": "0000000000000000000000000000000000000000000000000000000000000000"
            },{
              "address": "78P16M3XmFRGcWFCcsgt1WcTntA1jzcq31seQX1Eg92j8VQ99NPivmdKam4J5CKNAD7KuNWcq5xUPgoWczChzdba5WLwQ4j",
              "description": "Third account",
              "index": 1,
              "payment_id": "0000000000000000000000000000000000000000000000000000000000000000"
            }]
          }
        }
      ```  

---

---

###**`add_address_book`**

Add an entry to the address book.

Alias: None.

**Inputs:**

* **`address`** - *string*;

* **`payment_id`** - (optional) string, defaults to "0000000000000000000000000000000000000000000000000000000000000000";

* **`description`** - (optional) string, defaults to "";

**Outputs:**

* **`index`** - *unsigned int*; The index of the address book entry.

!!! example
    **`add_address_book`**

      ```
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"add_address_book","params":{"address":"78P16M3XmFRGcWFCcsgt1WcTntA1jzcq31seQX1Eg92j8VQ99NPivmdKam4J5CKNAD7KuNWcq5xUPgoWczChzdba5WLwQ4j","description":"Third account"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "index": 1
          }
        }
      ```  

---

---

###**`delete_address_book`**

Delete an entry from the address book.

**Inputs:**

* **`index`** - *unsigned int*; The index of the address book entry.

**Outputs:** 

* *None*

!!! example
    **`delete_address_book`**

      ```
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"delete_address_book","params":{"index":1}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
          }
        }
      ```

---

---

###**`refresh`**

Refresh a wallet after openning.

**Inputs:**

* **`start_height`** - **unsigned int*;* (Optional) The block height from which to start refreshing.

**Outputs:**

* **`blocks_fetched`** - *unsigned int*; Number of new blocks scanned.

* **`received_money`** - **boolean**; States if transactions to the wallet have been found in the blocks.

!!! example
    **`refresh`**

      ```
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"refresh","params":{"start_height":100000}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "blocks_fetched": 24,
            "received_money": true
          }
        }
      ```  

---

---

###**`rescan_spent`**

Rescan the blockchain for spent outputs.

**Inputs:** 

* *None*

**Outputs:** 

* *None*

!!! example
    **`rescan_spent`**

      ```
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"rescan_spent"}' -H 'Content-Type: application/json'

        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
          }
        }
      ```  

---

---

###**`start_mining`**

Start mining in the Amity daemon.

**Inputs:**

* **`threads_count`** - *unsigned int*; Number of threads created for mining.

* **`do_background_mining`** - **boolean**; Allow to start the miner in smart mining mode.

* **`ignore_battery`** - **boolean**; Ignore battery status (for smart mining only)

**Outputs:** 

* *None*

!!! example
    **`start_mining`**

      ```
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"start_mining","params":{"threads_count":1,"do_background_mining":true,"ignore_battery":false}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
          }
        }
      ```      

---

---

###**`stop_mining`**

Stop mining in the Amity daemon.

**Inputs:**

* *None*

**Outputs:** 

* *None*

!!! example
    **`stop_mining`**

      ```  
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"stop_mining"}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
          }
        }
      ```  

---

---

###**`get_languages`**

Get a list of available languages for your wallet's seed.

**Inputs:** 

* *None*

**Outputs:**

* **`languages`** - *array of string*; List of available languages

!!! example
    **`get_languages`**

      ```  
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_languages"}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "languages": ["Deutsch","English","Español","Français","Italiano","Nederlands","Português","русский язык","日本語","简体中文 (中国)","Esperanto","Lojban"]
          }
        }
      ```  

---

---

###**`create_wallet`**

Create a new wallet. You need to have set the argument "–wallet-dir" when launching amity-wallet-rpc to make this work.

**Inputs:**

* **`filename`** - *string*; Wallet file name.

* **`password`** - *string*; (Optional) password to protect the wallet.

* **`language`** - *string*; Language for your wallets' seed.

**Outputs:** 

* *None*

!!! example
    **`create_wallet`**

      ```
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"create_wallet","params":{"filename":"mytestwallet","password":"mytestpassword","language":"English"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
          }
        }
      ```

---

---

###**`open_wallet`**

Open a wallet. You need to have set the argument "–wallet-dir" when launching amity-wallet-rpc to make this work.

**Inputs:**

* **`filename`** - *string*; wallet name stored in –wallet-dir.

* **`password`** - *string*; (Optional) only needed if the wallet has a password defined.

**Outputs:** 

* *None*

!!! example
    **`open_wallet`**

      ```  
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"open_wallet","params":{"filename":"mytestwallet","password":"mytestpassword"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
          }
        }
      ```  

---

---

###**`close_wallet`**

Close the currently opened wallet, after trying to save it.

**Inputs:** 

* *None*

**Outputs:**

* *None*

!!! example
    **`close_wallet`**

      ```  
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"close_wallet"}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
          }
        }
      ```  

---

---

###**`change_wallet_password`**

Change a wallet password.

**Inputs:**

* **`old_password`** - *string*; (Optional) Current wallet password, if defined.

* **`new_password`** - *string*; (Optional) New wallet password, if not blank.

**Outputs:** 

* *None*

!!! example
    **`change_wallet_password`**

      ```  
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"change_wallet_password","params":{"old_password":"theCurrentSecretPassPhrase","new_password":"theNewSecretPassPhrase"}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
          }
        }
      ```

---

---

###**`is_multisig`**

Check if a wallet is a multisig one.

**Inputs:** 

* *None*

**Outputs:**

* **`multisig`** - **boolean**; States if the wallet is multisig

* **`ready`** - *boolean*;

* **`threshold`** - *unsigned int*; Amount of signature needed to sign a transfer.

* **`total`** - *unsigned int*; Total amount of signature in the multisig wallet.

!!! example
    **`is_multisig`**

    **Example for a non-multisig wallet:**

      ```
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"is_multisig"}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "multisig": false,
            "ready": false,
            "threshold": 0,
            "total": 0
          }
        }
      ``` 

    **Example for a multisig wallet:**

      ```  
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"is_multisig"}' -H 'Content-Type: application/json'                  {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "multisig": true,
            "ready": true,
            "threshold": 2,
            "total": 2
          }
        }
      ```  

---

---

###**`prepare_multisig`**

Prepare a wallet for multisig by generating a multisig string to share with peers.

**Inputs:** 

* *None*

**Outputs:**

* **`multisig_info`** - *string*; Multisig string to share with peers to create the multisig wallet.

!!! example
    **`prepare_multisig`**

      ```
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"prepare_multisig"}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "multisig_info": "MultisigV1BFdxQ653cQHB8wsj9WJQd2VdnjxK89g5M94dKPBNw22reJnyJYKrz6rJeXdjFwJ3Mz6n4qNQLd6eqUZKLiNzJFi3UPNVcTjtkG2aeSys9sYkvYYKMZ7chCxvoEXVgm74KKUcUu4V8xveCBFadFuZs8shnxBWHbcwFr5AziLr2mE7KHJT"
          }
        }
      ```

---

---

###**`make_multisig`**

Make a wallet multisig by importing peers multisig string.

**Inputs:**

* **`multisig_info`** - *array of string*; List of multisig string from peers.

* **`threshold`** - *unsigned int*; Amount of signatures needed to sign a transfer. Must be less or equal than the amount of signature in multisig_info.

* **`password`** - *string*; Wallet password

**Outputs:**

* **`address`** - *string*; multisig wallet address.

* **`multisig_info`** - *string*; Multisig string to share with peers to create the multisig wallet (extra step for N-1/N wallets).

!!! example
    **`make_multisig`**
    
    **Example for 2 / 2 Multisig Wallet:**

      ```  
        $ curl -X POST http://localhost:/9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"make_multisig","params":{"multisig_info":["MultisigV1K4tGGe8QirZdHgTYoBZMumSug97fdDyM3Z63M3ZY5VXvAdoZvx16HJzPCP4Rp2ABMKUqLD2a74ugMdBfrVpKt4BwD8qCL5aZLrsYWoHiA7JJwDESuhsC3eF8QC9UMvxLXEMsMVh16o98GnKRYz1HCKXrAEWfcrCHyz3bLW1Pdggyowop"],"threshold":2}}' -H 'Content-Type: application/json'
        { 
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "address": "55SoZTKH7D39drxfgT62k8T4adVFjmDLUXnbzEKYf1MoYwnmTNKKaqGfxm4sqeKCHXQ5up7PVxrkoeRzXu83d8xYURouMod",
            "multisig_info": ""
          }
        }
      ```


    **Example for 2 / 3 Multisig Wallet:**

      ```  
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"make_multisig","params":{"multisig_info":["MultisigV1MTVm4DZAdJw1PyVutpSy8Q4WisZBCFRAaZY7hhQnMwr5AZ4swzThyaSiVVQM5FHj1JQi3zPKhQ4k81BZkPSEaFjwRJtbfqfJcVvCqRnmBVcWVxhnihX5s8fZWBCjKrzT3CS95spG4dzNzJSUcjheAkLzCpVmSzGtgwMhAS3Vuz9Pas24","MultisigV1TEx58ycKCd6ADCfxF8hALpcdSRAkhZTi1bu4Rs6FdRC98EdB1LY7TAkMxasM55khFgcxrSXivaSr5FCMyJGHmojm1eE4HpGWPeZKv6cgCTThRzC4u6bkkSoFQdbzWN92yn1XEjuP2XQrGHk81mG2LMeyB51MWKJAVF99Pg9mX2BpmYFj"],"threshold":2}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "address": "51sLpF8fWaK1111111111111111111111111111111111ABVbHNf1JFWJyFp5YZgZRQ44RiviJi1sPHgLVMbckRsDkTRgKS",
            "multisig_info": "MultisigxV18jCaYAQQvzCMUJaAWMCaAbAoHpAD6WPmYDmLtBtazD654E8RWkLaGRf29fJ3stU471MELKxwufNYeigP7LoE4tn2Sscwn5g7PyCfcBc1V4ffRHY3Kxqq6VocSCUTncpVeUskaDKuTAWtdB9VTBGW7iG1cd7Zm1dYgur3CiemkGjRUAj9bL3xTEuyaKGYSDhtpFZFp99HQX57EawhiRHk3qq4hjWX"
          }
        }
      ```  

---

---

###**`export_multisig_info`**

Export multisig info for other participants.

**Inputs:** 

* *None*

**Outputs:**

* **`info`** - *string*; Multisig info in hex format for other participants.

!!! example
    **`export_multisig_info`**

      ```
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"export_multisig_info"}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "info": "7206578706f72740105cf4d6f6e65726f206d756c7469736966442b09b75f5eca9d846771fe1a879c9a97a26ac8601932415b0b6f4b0553ffbcec64b1148eb7832b51e7898d7944c41cee000415c5a98f4f80dc0efdae379a98f6f421cd03e129eb5b27d6e3b73eb69292019cbbfd712e47d01262c59980a8f9a8be776f2bf585f1477a6df63805bb6eacae743446d6364614d941ecfdcb6e958a390eb9aa770a0ce568c853206751a334507c1ae706c1a9ecdc87f056673d73bc7c5f0ab1f74a682e902e48a3322c0413bb7f6fd67404f13fb8e313fef490068d3c8ca0e"
          }
        }
      ```

---

---

###**`import_multisig_info`**

Import multisig info from other participants.

**Inputs:**

* **`info`** - *array of string*; List of multisig info in hex format from other participants.

**Outputs:**

* **`n_outputs`** - *unsigned int*; Number of outputs signed with those multisig info.

!!! example
    **`import_multisig_info`**

      ```  
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"import_multisig_info","params":{"info":["...multisig_info..."]}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "n_outputs": 35
          }
        }
      ```

---

---

###**`finalize_multisig`**

Turn this wallet into a multisig wallet, extra step for N-1/N wallets.

**Inputs:**

* **`multisig_info`** - *array of string*; List of multisig string from peers.

* **`password`** - *string*; Wallet password

**Outputs:**

* **`address`** - *string*; multisig wallet address.

!!! example
    **`finalize_multisig`**

      ```  
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"finalize_multisig","params":{"multisig_info":["MultisigxV1JNC6Ja2oBWt5Sqea9LN2YEF7RtU471MELKxwufNYeigP7YZCpHqr2EKvPG89Trf3X4E8LL9q15xNvf29fJ3stoE4WkLaGRPr4Sbn2McZT5uwC9YRr7UwjXqSZHmTWN9PBuZEKVAQ4HPPyQciSCdNjgwsuFRBzrskMdMUwNMgKst1debYfm37i6PSzDoS2tk4kYTYj83kkAdR7kdshet1axQPd6HQ","MultisigxV1Unma7Ko4xqq6VocSCUTncpVezdd8Ps3Af4oZwtj2JdWKzwNfP6s2G9ZvXhMoSRHY3KUskMcPscwn5g7PyCfcBc1V4ffr4SbL9q15xNvZT5uwC9YRr7UwjXqSZHmTWN9PBuZE1LTpWxLoC3vPMSrqVVcjnmL9LYfdCuY5jurQTcNoGhDTZz3fECjNZbCEDq3PHDiUio5WM9xaAdim9YByiS5KyqF4"]}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "address": "5B9gZUTDuHTcGGuY3nL3t8K2tDnEHeRVHSBQgLZUTQxtFYVLnho5JJjWJyFp5YZgZRQ44RiviJi1sPHgLVMbckRsDqDx1gV"
          }
        }
      ```  

---

---

###**`sign_multisig`**

Sign a transaction in multisig.

**Inputs:**

**`tx_data_hex`** - *string*; Multisig transaction in hex format, as returned by transfer under multisig_txset.

**Outputs:**

* **`tx_data_hex`** - *string*; Multisig transaction in hex format.

* **`tx_hash_list`** - *array of string*; List of transaction Hash.

!!! example
    **`sign_multisig`**

      ```
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"sign_multisig","params":{"tx_data_hex":"...multisig_txset..."}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "tx_data_hex": "...multisig_txset...",
            "tx_hash_list": ["4996091b61c1be112c1097fd5e97d8ff8b28f0e5e62e1137a8c831bacf034f2d"]
          }
        }
      ```

---

---

###**`submit_multisig`**

Submit a signed multisig transaction.

**Inputs:**

* **`tx_data_hex`** - *string*; Multisig transaction in hex format, as returned by sign_multisig under tx_data_hex.

**Outputs:**

* **`tx_hash_list`** - *array of string*; List of transaction Hash.

!!! example
    **`submit_multisig`**
        
      ```  
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"submit_multisig","params":{"tx_data_hex":"...tx_data_hex..."}}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
            "tx_hash_list": ["1b61cff8b28499609f08c831e1be112c1097fd5e97d85e62e1137abacf034f2d"]
          }
        }
      ```

---

---

###**`get_version`**

Get RPC version Major & Minor integer-format, where Major is the first 16 bits and Minor the last 16 bits.


**Inputs:** 

* *None*

**Outputs:**

* **`version`** - *unsigned int*; RPC version, formatted with `Major * 2^16 + Minor` (Major encoded over the first 16 bits, and Minor over the last 16 bits).

!!! example
    **`get_version`**

      ```
        $ curl -X POST http://localhost:9999/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_version"}' -H 'Content-Type: application/json'
        {
          "id": "0",
          "jsonrpc": "2.0",
          "result": {
          "version": 65539
          }
        }
      ```